

" ABOUT:        Vim-pairify plugin (completes for unclosed brackets and quotes)
" DESCRIPTION:  autoload functions lib file
" AUTHOR:       Dhruva Sagar <dhruva.sagar@gmail.com>
" WEBPAGE:      https://github.com/dhruvasagar/vim-pairify/
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>
" WEBPAGE:"     https://bitbucket.com/dsjkvf/vim-dhruvasagar-pairify

" Main

function! s:is_compliment(char1, char2)
    if has_key(g:pairs.left, a:char1)
        return a:char2 == g:pairs.left[a:char1]
    elseif has_key(g:pairs.right, a:char1)
        return a:char2 == g:pairs.right[a:char1]
    endif
endfunction

function! s:is_quote(char)
    return a:char ==# "'" || a:char ==# '"'
endfunction

function! s:find_pair(string)
    let stack = []
    let characters = split(a:string, '\zs')

    for char in reverse(characters)
        if has_key(g:pairs.right, char)
            if !empty(stack) && s:is_quote(char) && stack[-1] ==# char
                call remove(stack, -1)
                continue
            endif
            call add(stack, char)
        elseif has_key(g:pairs.left, char)
            if !empty(stack) && s:is_compliment(char, stack[-1])
                call remove(stack, -1)
            elseif empty(stack)
                return g:pairs.left[char]
            endif
        endif
    endfor

    if empty(stack)
        return "\t"
    else
        " the special case of Vim filetype comments (do not complete quote if at comment)
        if b:cur_ft =~? "vim" && b:cur_sn =~? "comment"
            return "\t"
        else
            return remove(stack, 0)
        endif
    endif
endfunction

function! pairify#pairify()
    " store the current syntax group and the current filetype into buffer variables for the later access
    let b:cur_sn = synIDattr(synID(line("."), col(".")-1, 1), "name")
    let b:cur_ft = &l:filetype
    " define the scope to look for an unclosed bracket or quote
    " if we are at the very first line, then the scope will be just this line
    if line('.') == 1
        let the_scope = getline('.')
    " if we are not far away from the beginning of the buffer (less lines than defined
    " via g:the_scope_size) then the scope will be from the 1 line till the current
    elseif line('.') < g:the_scope_size
        let the_scope = join(getline(0, '.'))
    " and if we are substantially away from the beginning of the buffer (more lines
    " than defined via g:the_scope_size), then the scope will be this very line and
    " g:the_scope_size lines back
    else
        let the_scope = join(getline(line('.') - g:the_scope_size, '.'))
    endif
    " search the scope
    return s:find_pair(the_scope[0:-1])
endfunction
