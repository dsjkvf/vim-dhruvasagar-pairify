
" DESCRIPTION:  Vim-pairify plugin (completes for unclosed brackets and quotes)
" AUTHOR:       Dhruva Sagar <dhruva.sagar@gmail.com>
" WEBPAGE:      https://github.com/dhruvasagar/vim-pairify/
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>
" WEBPAGE:"     https://bitbucket.com/dsjkvf/vim-dhruvasagar-pairify

" Check if script is loaded already, or if the user does not want it loaded

if exists('g:loaded_pairify')
    finish
endif
let g:loaded_pairify = 1

" Set the options

" set the size of the scope to look for an unclosed bracket or quote
if !exists('g:the_scope_size')
    let g:the_scope_size = 0
endif

" introduce brackets and quotes to complete
let g:pairs = {
            \ "left": {
            \       "[": "]",
            \       "(": ")",
            \       "{": "}",
            \       "<": ">",
            \       "'": "'",
            \       '"': '"'
            \ },
            \ "right": {
            \       "]": "[",
            \       ")": "(",
            \       "}": "{",
            \       ">": "<",
            \       "'": "'",
            \       '"': '"'
            \ }
            \}

" Mappings

" <Plug> mapping
inoremap <expr> <silent> <Plug>(pairify-complete) pairify#pairify()

" recommended mapping for $VIMRC
" imap <expr> <Tab> pumvisible()
"     \ ? "\<C-n>"
"     \ : "<Plug>(pairify-complete)"
