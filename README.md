# vim-pairify

## About

This is a Vim plugin for pair completion such as `()` and `[]`. Originally written by [Dhruva Sagar](https://github.com/dhruvasagar/), forked now from [its GitHub page](https://github.com/dhruvasagar/vim-pairify) because of several additions.

## Changes

Instead of `<C-j>` mapping, the plugin now is adapted to work with `<Tab>`. Besides that, instead of looking for pairs to complete in the current line only, the plugin now can be configured to extend this scope.

## Mappings

In order to work correctly with the [auto]completion, the following conditional mapping is recommended:

    imap <expr> <Tab> pumvisible()
    \ ? "\<C-n>"
    \ : "<Plug>(pairify-complete)"

which will return `<C-n>` if the popup menu is visible, and will run the plugin otherwise (the plugin, in return, will either complete an unclosed pair, or, if there isn't one, will insert `<Tab>`).

## Configuration

To configure the scope to look for an uncompleted pair, (default is disabled, set to `0`):

    let g:pairify_scope = 10

## Conclusion

For other details, please, read the original [README](https://bitbucket.org/dsjkvf/vim-dhruvasagar-pairify/src/master/README.orig) -- and if the introduced changes aren't of great importance to you, I also strongly recommend to use the original version, too.
